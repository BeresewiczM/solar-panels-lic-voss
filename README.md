# Detect solar panels with turicreate

Repo with sample aerial photos of solar panels and object detection model developed with Turi Create

Model developed in Michel's Voss Bachelor's Theisis [link](https://github.com/DepartmentOfStatisticsPUE/prace-dyplomowe/blob/master/licencjat/2019-voss112124lic.pdf).

## Repo structure

`model` -- folder with object detection model developed with turicreate
`figs` -- sample aerial photos from 2016 and 2018
`results` -- sample results with bounding boxes


## Run codes 

1. Enable environment

See tutorial at https://github.com/apple/turicreate

2. Prediction

```{python}
## import modules
import turicreate as tc
import pandas as pd
from PIL import Image

## load model
model=tc.load_model("model/model_aws")

## load images
imgs = tc.image_analysis.load_images("figs/","auto", with_path=True)

## prediction
preds = model.predict(imgs)

## save results
imgs['predictions'] = preds
imgs['image_with_predictions'] = tc.object_detector.util.draw_bounding_boxes(imgs['image'], imgs['predictions'])
# imgs[['image', 'image_with_predictions']].explore()

## count correct predictions
d=[len(i) >0 for i in preds]
df=pd.DataFrame({"true": True, "pred": d})
df["year"]=imgs['path'].apply(lambda path: '2018' if '_2018' in path else '2016')
pd.crosstab([df.year], [df.true,df.pred], margins=True)

## save results to file
nrow,ncol=imgs.shape

for i in range(nrow):
    print(i)
    fname = os.path.basename(imgs['path'][i])
    Image.fromarray(imgs['image_with_predictions'][i].pixel_data).save("results/" + fname)

```

## How to cite


```
@thesis{voss2019,
  author       = {Voss, Michel}, 
  title        = {Detection of solar panels based on aerial images of the city of Poznań using deep neural networks},
  school       = {{Poznan University of Economics and Business}},
  year         = 2019,
  note         = {Bachelor's Thesis (in Polish)},
  url = {https://github.com/DepartmentOfStatisticsPUE/prace-dyplomowe/blob/master/licencjat/2019-voss112124lic.pdf}
}
```

and


```
@unpublished{voss-beresewicz,
title= {Detection of solar panels based on aerial images of the city of Poznań using deep neural networks},
author = {Voss, Michel and Beręsewicz, Maciej},
year = {2019},
note= {Methodology of Statistical Research, MET 2019, 3-5 July 2019, Warsaw},
URL= {https://met2019.stat.gov.pl/en/},
}
```


